package com.springtest.demo.Controller;

import com.springtest.demo.Model.User;
import com.springtest.demo.repository.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
//Autowired-to do constructor injection
//@Configuration: allow to register extra beans in the context or import additional configuration	classes
//RestController- this annotation is applied to a class to mark it as a request handler.
// Spring RestController annotation is used to create RESTful web services using Spring MVC.

@RestController
@RequestMapping("/users")

public class Usercontroller {

    @Autowired
    private UserRepository userRepository;





  /*  @PostMapping("/create")
public Mono<ResponseEntity<User>> create(@RequestBody User user){
    return userRepository.save(user)
            .map(savedUser -> ResponseEntity.ok(savedUser));
}*/

    @GetMapping("/all")
    public Flux<User> getAll() {
        return userRepository.findAll();
    }


    @PostMapping("/createuser")
    public Mono<ResponseEntity<User>> create(@RequestHeader(value = "Authorization") String authorization,
                                             @RequestHeader(value = "API_KEY") String api_key,
                                             @RequestBody User user) {

        return userRepository.save(user).map(savedUser -> ResponseEntity.ok(savedUser));
    }

    @PutMapping("/updateuser/{userId}")
    public Mono<ResponseEntity<User>> update(@PathVariable Integer userId, @RequestBody User user) {



           return userRepository.findById(userId).
                    flatMap(dbUser -> { user.setName(user.getName());
                    user.setSection(user.getSection());
                    return userRepository.save(user);
                    })
                    .map(updatedUser -> ResponseEntity.ok(updatedUser))
                    .defaultIfEmpty(ResponseEntity.badRequest().build());


    }

    @DeleteMapping("/deleteuser/{userId}")
    public Mono<ResponseEntity<Void>> delete(@PathVariable Integer userId ) {

        return userRepository.findById(userId)
                .flatMap(existingUser ->userRepository
                .delete(existingUser)
                .then(Mono.just(ResponseEntity.ok().<Void>build()))
        )
                .defaultIfEmpty(ResponseEntity.notFound().build());

    }
}










