package com.springtest.demo.Service.dto;


import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Optional;

@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SampleDto {

    private String authorization;
    private String banner;

    public Optional<String> getAuthorization() {
        return Optional.ofNullable(authorization);
    }

    public Optional<String> getBanner() {
        return Optional.ofNullable(banner);
    }


}
