package com.springtest.demo.repository;


import com.springtest.demo.Model.User;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

// we get set of methods, specifications and implementations.
@Repository
public interface UserRepository extends ReactiveMongoRepository<User, Integer> {



}

