package com.springtest.demo.component;

import com.springtest.demo.Controller.Usercontroller;
import com.springtest.demo.Model.User;
import com.springtest.demo.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.IfProfileValue;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.test.web.servlet.htmlunit.UrlRegexRequestMatcher;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import static org.mockito.Mockito.when;

//@Autowired: Spring provides annotation-based auto-wiring by providing @Autowired annotation.
// It is used to autowire spring bean on setter methods, instance variable, and constructor.
// When we use @Autowired annotation, the spring container auto-wires the bean by matching data-type.

//@Mockbean-You can use the annotation to add new beans or replace a single existing bean definition
//@RunWith(SpringRunner. class) is used to provide a bridge between Spring Boot test features and JUnit.
  //      Whenever we are using any Spring Boot testing features in our JUnit tests, this annotation will be required.

//@WebFluxTest-Using this annotation will disable full auto-configuration and instead apply only configuration relevant to WebFlux tests
// (i.e. @Controller, @ControllerAdvice, @JsonComponent, Converter/GenericConverter, and WebFluxConfigurer beans but not @Component,
// @Service or @Repository beans).

//@ContextConfiguration defines class-level metadata that is used to determine
  //      how to load and configure an ApplicationContext for integration tests.

@RunWith(SpringRunner.class)
@WebFluxTest(Usercontroller.class)
@Slf4j
@ContextConfiguration

public class UsercomponentTest {

    @Autowired
    WebTestClient webTestClient = WebTestClient.bindToServer().build() ;

    @Autowired
    private Usercontroller usercontroller;

    @MockBean
    UserRepository userRepository;

    @Test
    public void isgetUserStatusisOK()  {
        User user=new User();
        user.setId("1");
        user.setName("qwe");
        user.setSection("dev");
        Flux<User> userMono = Flux.just(user);

        when(usercontroller.getAll()).thenReturn(userMono);

       webTestClient.get().uri("/users/all").accept(MediaType.APPLICATION_JSON)
                .exchange().expectStatus().isOk().expectBody().consumeWith(response ->
               Assertions.assertThat(response.getResponseBody()).isNotNull());
       log.info("testcase passed");
               //.expectBody(User.class).value(user1 -> user.getName().equals("sree"));
    }

    @Test
    public void iscreateUserStatusisOK()  {
    }
    @Test
    public void isupdateUserStatusisOK()  {
    }


}
