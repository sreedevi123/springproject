package com.springtest.demo.functional;

import java.io.IOException;

public interface IUserFunctionalTest {
    public void testCreateUser() throws IOException;
    public void testgetUser();
    public void testUpdateUser();
    public void testdeleteUser();
    public void testCreatewithinvalidid();
    public void testupdatewithinvalidid();
    public void testdeleteUserwithinvalidid();


}
