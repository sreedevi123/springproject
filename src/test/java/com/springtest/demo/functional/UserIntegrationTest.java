package com.springtest.demo.functional;

import com.springtest.demo.Model.User;
import com.springtest.demo.functional.base.AbstractBaseIntegrationTest;
import org.apache.commons.logging.Log;
import org.assertj.core.api.Assertions;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.ConfigFileApplicationContextInitializer;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.reactive.ClientHttpConnector;
import org.springframework.test.annotation.IfProfileValue;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.reactive.server.WebTestClient;

import reactor.core.publisher.Mono;

import java.io.IOException;
import java.time.Duration;
import java.util.Properties;

//Annotation that can be applied to a test class to enable a WebTestClient . At the moment, only WebFlux applications are supported.

@Slf4j
public class UserIntegrationTest extends AbstractBaseIntegrationTest implements IUserFunctionalTest{

     Usertest usertest = new Usertest();

    @Test
    @IfProfileValue(name = "test-groups", values = {"integration-test"})
    public void testCreateUser() throws IOException {
        usertest.validateCreateUser( "user.json");
    }

    @Test
    @IfProfileValue(name = "test-groups", values = {"integration-test"})
    public void testgetUser() {
        usertest.validateGetUser();

    }

    @Test
    @IfProfileValue(name = "test-groups", values = {"integration-test"})
    public void testUpdateUser() {
        usertest.validateUpdateUser();
    }


    @Test
    @IfProfileValue(name = "test-groups", values = {"integration-test"})
    public void testdeleteUser() {
        usertest.validateDeleteUser();
    }

    //negative scenario-create user
    @Test
    @IfProfileValue(name = "test-groups", values = {"integration-test"})
    public void testCreatewithinvalidid() {
        usertest.validateCreatewithinvalidid();

    }

    //negative scenario

    @Test
    @IfProfileValue(name = "test-groups", values = {"integration-test"})
    public void testupdatewithinvalidid() {
        usertest.validupdatewithinvalidid();

    }

    @Test
    @IfProfileValue(name = "test-groups", values = {"integration-test"})
    public void testdeleteUserwithinvalidid() {
        usertest.validatedeleteUserwithinvalidid();
    }


}
