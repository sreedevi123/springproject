package com.springtest.demo.functional;

import com.springtest.demo.Model.User;
import com.springtest.demo.functional.base.AbstractBaseIntegrationTest;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.IfProfileValue;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.BodyInserters;
import reactor.core.publisher.Mono;

import java.io.IOException;
import java.nio.file.Paths;
import java.time.Duration;

import static java.nio.file.Files.readAllBytes;

@Slf4j
public class Usertest extends AbstractBaseIntegrationTest {
    @Autowired
    protected WebTestClient webTestClient=WebTestClient.bindToServer()
            .baseUrl("http://localhost:8081/users")
            .responseTimeout(Duration.ofMillis(480000))
            .build();

    public static final String DATA_FILE_PATH = "src/test/java/com/springtest/demo/functional/data/";

    public void validateCreateUser( String payloadFile) throws IOException {

        String dataFile = DATA_FILE_PATH+payloadFile;
        String dataJson= new String(readAllBytes(Paths.get(dataFile)), "UTF-8");
        dataJson=dataJson.replace("$id$",randomNumber());

    //User userRequest = new User(id, "swamynath", "SE");
        log.info("invoking create API");
        /*webTestClient.post()
                .uri("/createuser")
                .body(Mono.just(userRequest), User.class).
                exchange().
                expectStatus().
                is2xxSuccessful();*/

        webTestClient.post()
                .uri("/createuser").headers(httpHeaders ->{httpHeaders.add("Authorization", "testuser");
        httpHeaders.add("API_KEY", "12345"); }).contentType(MediaType.APPLICATION_JSON)
            .body(BodyInserters.fromValue(dataJson)).
    exchange().expectStatus().isEqualTo(HttpStatus.OK);

    }

    public void validateGetUser()
    {
        webTestClient.get()
                .uri("/all")
                .exchange()
                .expectStatus()
                .is2xxSuccessful();
    }

    public void validateUpdateUser() {
        User userRequest = new User("16", "test", "we");
        webTestClient.put().
                uri("/updateuser/{userId}", "12")
                .body(Mono.just(userRequest), User.class)
                .exchange().
                expectStatus().
                is2xxSuccessful();
    }

    public void validateDeleteUser() {
        webTestClient.delete().
                uri("/deleteuser/{userId}", "13")
                .exchange().
                expectStatus().
                is2xxSuccessful();


    }
    public void validateCreatewithinvalidid() {
        User userRequest = new User();
        userRequest.setName("qwe");
        userRequest.setSection("dev");
        webTestClient.post()
                .uri("/createuser")
                .body(Mono.just(userRequest), User.class).
                exchange().
                expectStatus().
                is2xxSuccessful();

        webTestClient.post()
                .uri("/createuser")
                .body(Mono.just(userRequest), User.class)
                .exchange()
                .expectStatus()
                .isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    public void validupdatewithinvalidid() {

        User userRequest = new User("007", "test", "we");
        webTestClient.put().
                uri("/updateuser/{userId}", "007")
                .body(Mono.just(userRequest), User.class)
                .exchange()
                .expectStatus()
                .isEqualTo(HttpStatus.BAD_REQUEST);
    }
    @Test
    @IfProfileValue(name = "test-groups", values = {"integration-test"})
    public void validatedeleteUserwithinvalidid() {
        //User userRequest = new User(12, "test", "we");
        webTestClient.delete().
                uri("/deleteuser/{userId}", "100")
                .exchange().
                expectStatus().isEqualTo(HttpStatus.NOT_FOUND);
    }

}
