package com.springtest.demo.functional.base;

import org.junit.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.ConfigFileApplicationContextInitializer;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.reactive.server.WebTestClient;


import java.time.Duration;
@Configuration
@ContextConfiguration(initializers = ConfigFileApplicationContextInitializer.class)
@ActiveProfiles(profiles = {"native", "integration-test"})
//@PropertySource("classpath:application-test.properties")

public abstract class AbstractBaseIntegrationTest {

    @Autowired
   // protected FunctionalTestproperties functionalTestProperties;
    protected WebTestClient webTestClient;


    @Before
    public void setUp() {
        this.webTestClient = WebTestClient.bindToServer()
                .baseUrl("http://localhost:8081/users")
                .responseTimeout(Duration.ofMillis(480000))
                .build();
    }

    public String randomNumber()
    {
        String id = String.valueOf(Math.round(Math.random() * 50) + 50);
        return id;
    }
}
